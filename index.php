<?php
	include_once("includes/application_top.php");
	
	if(isset($_GET['_page']))
	{
		$contentPage = $_GET['_page'];	
	}else{
		$contentPage = "home";	
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Beauty Room</title>
<link rel="stylesheet" href="control/styles.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" />
<link type="image/png" href="favi1.png" rel="icon">
<script type="text/javascript">
$(document).ready(function(e) {
    $(".menu li").click(function(){
		
	});
	$('li').hover(function(){
		$(this).find('.submenu').stop().slideToggle(200);
	}); 
});
$('.fancybox').fancybox({
				openEffect	: 'elastic',
				closeEffect	: 'elastic',
				
			});
			$("a[rel=gallery_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
			$("a[rel=featured_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
</script>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script><!-- FOR FACEBOOK LIKE BOX -->

<div id="header">
	<div class="logo">
		<a href="index.php"><img src="images/logo.jpg" /> </a>
	</div>
</div><!--HEADER DIVISION -->

<div id="menu-wrapper">
	<?php include_once("view/menu.php"); ?>
</div><!-- MENU WRAPPER -->

<?php
	if( $contentPage == "home" )
	{
		?>
        
<div id="banner-wrapper">
	  <?php include_once("view/banner.php"); ?>
</div><!-- BANNER WRAPPER --> 
<?php } ?>
       
        <div id="main">
	<?php include_once("pages/".$contentPage.".php"); ?>

</div><!-- MAIN DIVISION -->

<div id="footer">
	<?php include_once("view/footer.php"); ?>
    </footer><!-- FOOTER -->
</body>
</html>